import { ref } from "vue";
import { defineStore } from "pinia";

export const calMathStore = defineStore("calStore", () => {
  const total = ref(0);

  const increase = () => {
    if (total.value >= 0) {
      total.value += 1;
    }
  };
  
  const decrease = () => {
    if (total.value > 0) {
      total.value -= 1;
    }
  };

  return { total, increase, decrease };
});
